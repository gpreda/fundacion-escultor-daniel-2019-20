{{--
  Template Name: Galeria Template
--}}

@extends('layouts.galeria')

@section('content') 
  {{-- galeria --}}
  @include('partials.galeria.content')
@endsection
