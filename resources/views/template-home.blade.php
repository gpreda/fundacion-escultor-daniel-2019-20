{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  {{-- hero  --}}
  @include('partials.home.hero')
  {{-- actores --}}
  @include('partials.home.actores')
  {{-- timeline --}}
  @include('partials.home.timeline')
  {{-- video --}}
  @include('partials.home.video')
  {{-- patronato --}}
  @include('partials.home.patronato')
@endsection
