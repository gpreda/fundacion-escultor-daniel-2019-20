<?php
$logos_representacion = get_field('logos_representacion_patronato');
$logos_colaboraciones = get_field('logos_colaboraciones_patronato');
?>
<div class="c-patronato">
        <h2 class="c-patronato__title">{{ the_field('titulo_principal_patronato') }}</h2>
        <div class="c-patronato__group">
            <p class="c-patronato__category">{{ the_field('titulo_familia_patronato') }}</p>
            <div class="c-patronato__wrapper">
                <p class="c-patronato__text">{{ the_field('copy_familia_patronato') }}</p>
            </div>
        </div>

        <div class="c-patronato__group">
            <p class="c-patronato__category">{{ the_field('titulo_representacion_patronato') }}</p>
            <div class="c-patronato__wrapper">
                @foreach ($logos_representacion as $logo_r)
                    <img src="{{$logo_r['logo_representacion']['url']}}" class="c-patronato__image">
                @endforeach
            </div>
        </div>

        <div class="c-patronato__group">
            <p class="c-patronato__category">{{ the_field('titulo_personal_patronato') }}</p>
            <div class="c-patronato__wrapper">
                <p class="c-patronato__text">{{ the_field('copy_personal_patronato') }}</p>
            </div>
        </div>

        <h2 class="c-patronato__title">{{ the_field('titulo_colaboraciones_patronato') }}</h2>
        <div class="c-patronato__group">
            <div class="c-patronato__wrapper">

                @foreach ($logos_colaboraciones as $logo_c)
                    <img src="{{$logo_c['logo_colaboraciones']['url']}}" class="c-patronato__image">
                @endforeach

            </div>
        </div>

    </div>



