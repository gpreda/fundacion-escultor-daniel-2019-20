<?php
$fondo_desktop = get_field('fondo_hero_desktop');
$fondo_mobile = get_field('fondo_hero_mobile');
$cta_hero = get_field('cta_hero');
?>
<div id="inicio" class="c-hero c-hero--centered">
    <div class="c-hero__container">
         <div class=" c-hero__content--copy">
             <h1 class="c-hero__title">{{ the_field('titulo_hero') }}</h1>
             <p class="c-hero__subtitle">{{ the_field('subtitulo_hero') }}</p>
             <?php if ($cta_hero != ""): ?>
                <a class="o-button o-button--inline" target="{{ $cta_hero['target'] }}" href="{{ $cta_hero['url'] }}">{{ $cta_hero['title'] }}</a>
             <?php endif ?>
         </div>
     </div>

     <div class="c-hero__background-image js-object-fit">
         <?php if(false){ ?>
             <img
             class="lazyload"
             alt="Escultor Daniel"
             data-src="{{ $fondo_desktop['url'] }}"
             data-sizes="100vw"
             data-srcset="{{ $fondo_mobile['url'] }} 600w,
             {{ $fondo_desktop['url'] }} 1440w"    />
         <?php } ?>
         <img class="c-hero__background-image--hidden-xs" src="{{ $fondo_desktop['url'] }}" alt="Escultor Daniel">
         <img class="c-hero__background-image--hidden" src="{{ $fondo_mobile['url'] }}" alt="Escultor Daniel">
     </div>
 </div>