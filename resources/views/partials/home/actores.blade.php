<?php 
$fondo1 = get_field('fondo_actor_1');
$fondo2 = get_field('fondo_actor_2');
$fondo3 = get_field('fondo_actor_3');
$fondo4 = get_field('fondo_actor_4');
$fondo5 = get_field('fondo_actor_5');
?>
<div id="sinopsis" class="c-actores" data-section="#sinopsis">
    <div class="c-actores--left" style="">
        <div class="c-actores__sinopsis" style="">
            <h2 class="c-actores__title">{{ the_field('titulo_introduccion') }}</h2>
            <div class="c-actores__body">{!! the_field('descripcion_introduccion') !!}</div>
        </div>
        
        <div class="c-actores__card c-actores__card--alberto c-actores__card--nopadding c-actores__card--hide-mobile" style="">
            <div class="c-actores__card-overlay"></div>
            <div class="c-actores__card-background-image js-object-fit">
                <img
                class="lazyload"
                alt="{{ the_field('titulo_actor_1') }}"
                data-src="{{ $fondo1['url'] }}' ?>"
                data-sizes="auto"
                data-srcset="{{ $fondo1['url'] }} 768w,
                {{ $fondo1['url'] }} 1440w"
                />
            </div>
            
            <div class="o-card o-card--alberto">
                <div class="o-card__top"></div>
                <div class="o-card__left"></div>
                <div class="o-card__bottom"></div>
                <div class="o-card__right"></div>
                <div class="o-card__container">
                    <h3 class="o-card__title">{{ the_field('titulo_actor_1') }}</h3>
                    <p class="o-card__subtitle">{{ the_field('subtitulo_actor_1') }}</p>
                    <div class="o-card__body">{!! the_field('descripcion_actor_1') !!}</div>
                </div>
            </div>
        </div>
        <div class="c-actores__card c-actores__card--javier c-actores__card--hide-mobile" style=" ">
            <div class="c-actores__card-overlay"></div>
            <div class="c-actores__card-background-image js-object-fit">
                <img
                class="lazyload"
                alt="{{ the_field('titulo_actor_2') }}"
                data-src="{{ $fondo2['url'] }}"
                data-sizes="auto"
                data-srcset="{{ $fondo2['url'] }} 768w,
                {{ $fondo2['url'] }} 1440w"
                />
            </div>
            
            <div class="o-card o-card--javier">
                <div class="o-card__top"></div>
                <div class="o-card__left"></div>
                <div class="o-card__bottom"></div>
                <div class="o-card__right"></div>
                <div class="o-card__container">
                    <h3 class="o-card__title">{{ the_field('titulo_actor_2') }}</h3>
                    <p class="o-card__subtitle">{{ the_field('subtitulo_actor_2') }}</p>
                    <div class="o-card__body">{!! the_field('descripcion_actor_2') !!}</div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="c-actores--right" style="">
        <div class="c-actores__card c-actores__card--najwa c-actores__card--hide-mobile" style=" ">
            <div class="c-actores__card-overlay"></div>
            <div class="c-actores__card-background-image js-object-fit">
                <img
                class="lazyload"
                alt="{{ the_field('titulo_actor_3') }}"
                data-src="{{ $fondo3['url'] }}"
                data-sizes="auto"
                data-srcset="{{ $fondo3['url'] }} 768w,
                {{ $fondo3['url'] }} 1440w"
                />
            </div>
            <div class="o-card o-card--najwa">
                <div class="o-card__top"></div>
                <div class="o-card__left"></div>
                <div class="o-card__bottom"></div>
                <div class="o-card__right"></div>
                <div class="o-card__container">
                    <h3 class="o-card__title">{{ the_field('titulo_actor_3') }}</h3>
                    <p class="o-card__subtitle">{{ the_field('subtitulo_actor_3') }}</p>
                    <div class="o-card__body">{!! the_field('descripcion_actor_3') !!}</div>
                </div>
            </div>
        </div>
        
        <div class="c-actores__card c-actores__card--q8 c-actores__card--hide-mobile" style="">
            <div class="c-actores__card-overlay"></div>
            <div class="c-actores__card-background-image js-object-fit">
                <img
                class="lazyload"
                alt="{{ the_field('titulo_actor_4') }}"
                data-src="{{ $fondo4['url'] }}"
                data-sizes="auto"
                data-srcset="{{ $fondo4['url'] }} 768w,
                {{ $fondo4['url'] }} 1440w"
                />
            </div>
            
            <div class="o-card o-card--q8">
                <div class="o-card__top"></div>
                <div class="o-card__left"></div>
                <div class="o-card__bottom"></div>
                <div class="o-card__right"></div>
                <div class="o-card__container">
                    <h3 class="o-card__title">{{ the_field('titulo_actor_4') }}</h3>
                    <p class="o-card__subtitle">{{ the_field('subtitulo_actor_4') }}</p>
                    <div class="o-card__body">{!! the_field('descripcion_actor_4') !!}</div>
                    <!--<div class="o-card__button">
                        <a href="#" class="o-button o-button--alt o-button--inline o-button--hove" target="_blank"><span>Descúbrelo</span></a>
                    </div>-->
                </div>
            </div>
        </div>
        <div class="c-actores__card c-actores__card--Kike c-actores__card--nopadding c-actores__card--hide-mobile">
            <div class="c-actores__card-overlay"></div>
            <div class="c-actores__card-background-image js-object-fit">
                <img
                class="lazyload"
                alt="{{ the_field('titulo_actor_5') }}"
                data-src="{{ $fondo5['url'] }}"
                data-sizes="auto"
                data-srcset="{{ $fondo5['url'] }} 768w,
                {{ $fondo5['url'] }} 1440w"
                />
            </div>
            
            <div class="o-card o-card--Kike">
                <div class="o-card__top"></div>
                <div class="o-card__left"></div>
                <div class="o-card__bottom"></div>
                <div class="o-card__right"></div>
                <div class="o-card__container">
                    <h3 class="o-card__title">{{ the_field('titulo_actor_5') }}</h3>
                    <p class="o-card__subtitle">{{ the_field('subtitulo_actor_5') }}</p>
                    <div class="o-card__body">{!! the_field('descripcion_actor_5') !!}</div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="c-actores__mobile">
        <div class="c-actores__mobile-item">
            <div class="c-actores__mobile-item__top js-actores-open">
                <div class="c-actores__mobile-item__image js-object-fit">
                    <img src="{{ $fondo1['url'] }}' ?>" alt="{{ the_field('titulo_actor_1') }}">
                </div>
            </div>
            <div class="c-actores__mobile-item__bottom">
                <div class="c-actores__mobile-item__content">
                    <h4 class="c-actores__mobile-item__title">{{ the_field('titulo_actor_1') }}</h4>
                    <span class="c-actores__mobile-item__subtitle">{{ the_field('subtitulo_actor_1') }}</span>
                    <div class="c-actores__mobile-item__copy">
                        {!! the_field('descripcion_actor_1') !!}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="c-actores__mobile-item">
            <div class="c-actores__mobile-item__top js-actores-open">
                <div class="c-actores__mobile-item__image js-object-fit">
                    <img src="{{ $fondo2['url'] }}" alt="{{ the_field('titulo_actor_2') }}">
                </div>
            </div>
            <div class="c-actores__mobile-item__bottom">
                <div class="c-actores__mobile-item__content">
                    <h4 class="c-actores__mobile-item__title">{{ the_field('titulo_actor_2') }}</h4>
                    <span class="c-actores__mobile-item__subtitle">{{ the_field('subtitulo_actor_2') }}</span>
                    <div class="c-actores__mobile-item__copy">{!! the_field('descripcion_actor_2') !!}</div>
                    <!-- <div class="o-card__button">
                        <a href="#" class="o-button o-button--alt o-button--inline o-button--hove" target="_blank"><span>Descúbrelo</span></a>
                    </div>-->
                </div>
            </div>
        </div>
        
        <div class="c-actores__mobile-item">
            <div class="c-actores__mobile-item__top js-actores-open">
                <div class="c-actores__mobile-item__image js-object-fit">
                    <img src="{{ $fondo3['url'] }}" alt="{{ the_field('titulo_actor_3') }}">
                </div>
            </div>
            <div class="c-actores__mobile-item__bottom">
                <div class="c-actores__mobile-item__content">
                    <h4 class="c-actores__mobile-item__title">{{ the_field('titulo_actor_3') }}</h4>
                    <span class="c-actores__mobile-item__subtitle">{{ the_field('subtitulo_actor_3') }}</span>
                    <div class="c-actores__mobile-item__copy">
                        {!! the_field('descripcion_actor_3') !!}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="c-actores__mobile-item">
            <div class="c-actores__mobile-item__top js-actores-open">
                <div class="c-actores__mobile-item__image js-object-fit">
                    <img src="{{ $fondo4['url'] }}" alt="{{ the_field('titulo_actor_4') }}">
                </div>
            </div>
            <div class="c-actores__mobile-item__bottom">
                <div class="c-actores__mobile-item__content">
                    <h4 class="c-actores__mobile-item__title">{{ the_field('titulo_actor_4') }}</h4>
                    <span class="c-actores__mobile-item__subtitle">{{ the_field('subtitulo_actor_4') }}</span>
                    <div class="c-actores__mobile-item__copy">
                        {!! the_field('descripcion_actor_4') !!}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="c-actores__mobile-item">
            <div class="c-actores__mobile-item__top js-actores-open">
                <div class="c-actores__mobile-item__image js-object-fit">
                    <img src="{{ $fondo5['url'] }}" alt="{{ the_field('titulo_actor_5') }}">
                </div>
            </div>
            <div class="c-actores__mobile-item__bottom">
                <div class="c-actores__mobile-item__content">
                    <h4 class="c-actores__mobile-item__title">{{ the_field('titulo_actor_5') }}</h4>
                    <span class="c-actores__mobile-item__subtitle">{{ the_field('subtitulo_actor_5') }}</span>
                    <div class="c-actores__mobile-item__copy">
                        {!! the_field('descripcion_actor_5') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
