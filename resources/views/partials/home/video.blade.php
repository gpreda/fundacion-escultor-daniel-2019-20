<?php 
$imagen_video = get_field('imagen_video');
?>
<div class="c-video">   
    <div class="c-video__content">
        <div class="c-video__wrapper">
            <a href="{{ the_field('youtube_enlace') }}" data-lity="">
                <img src="{{ $imagen_video['url'] }}">
            </a>            
        </div>    
    </div>    
</div>
