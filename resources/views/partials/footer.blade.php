<div class="c-footer">  

    <div class="u-wrapper"">
        
        <div class="c-footer__container" style="">
            
            <img src="{{ $opciones_generales['logo_footer']['url'] }}" class="c-footer__logo" style="min-width: 150px">
            
            <div class="c-footer__donacion" style="">                
                <p class="c-footer__text">{{ $opciones_generales['copy_cuenta'] }}</p>
                <h3 class="c-footer__title" style="font-size: 21px">{{ $opciones_generales['footer_numero_cuenta'] }}</h3>                
            </div>
            
            <div class="c-footer__contacto">
                
                <h3 class="c-footer__title">Contacto</h3>
                
                <p class="c-footer__text">Email: <a href="mailto:{{ $opciones_generales['footer_email_contacto'] }}" target="_blank">{{ $opciones_generales['footer_email_contacto'] }}</a></p>
                
                <p class="c-footer__text">Teléfono: {{ $opciones_generales['footer_telefono_contacto'] }}</p>
                
                <ul>                        
                    <li style="display: inline-block; width: 40px; padding: 0 5px">
                        <a href="{{ $opciones_generales['footer_enlace_facebook'] }}" target="_blank">
                            <img src="{{ $opciones_generales['footer_logo_facebook']['url'] }}"  style="width: 50px:">
                        </a>
                    </li>
                    <li style="display: inline-block; width: 40px; padding: 0 5px">
                        <a href="{{ $opciones_generales['footer_enlace_instagram'] }}" target="_blank">
                            <img src="{{ $opciones_generales['footer_logo_instagram']['url'] }}"  style="width: 50px:">
                        </a>
                    </li>
                    <li style="display: inline-block; width: 40px; padding: 0 5px">
                        <a href="{{ $opciones_generales['footer_enlace_youtube'] }}" target="_blank">
                            <img src="{{ $opciones_generales['footer_logo_youtube']['url'] }}" style="width: 50px:">
                        </a>
                    </li>

                </ul>

            </div>
        </div>

    </div>
    
</div>