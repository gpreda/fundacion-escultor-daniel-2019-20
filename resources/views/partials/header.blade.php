@php
    if($post->post_name == 'galeria') {
      $extra__class="c-header--black";
    } else {
      $extra__class="";      
    }    
@endphp



<header class="c-header c-header--fixed {{ $extra__class }}">
  <div class="c-header__container u-wrapper u-wrapper--stretch">
      <a class="c-header__logo" href="{{ home_url('/') }}"><img class="js-inlinesvg" src="{{ $opciones_generales['logo_header']['url'] }}" alt="Fundación Escultor Daniel"></a>
      
      <div class="u-hide u-show@tablet-wide">
          <div class="c-hamburger js-hamburger js-menu-toggle">
              <span></span>
              <span></span>
          </div>
      </div>
      
      <?php wp_nav_menu(array(
      'container' => '',
      'menu' => 'primary_navigation',
      'menu_id' => 'menu-nav',
      'menu_class' => 'c-menu js-menu',
      'link_before' => '',
      'link_after' => ''
      ));
      ?>
  </div> 
</header>
     