<div class="c-header__black">    
    <a class="brand" href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}"><img src="{{ $opciones_generales['logo_header']['url'] }}" class="c-header__black--logo"></a>
   <div class="c-menu">
        <nav class="nav-primary">
                @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
                @endif
            </nav>
   </div>
</div>