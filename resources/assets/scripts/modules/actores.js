/* eslint-disable */
jQuery(document).ready(function ($) {
    jQuery('.card').click(function (event) {
        event.preventDefault();

        if (jQuery(this).find(".btn-link").attr('aria-expanded') == 'true') {
            jQuery('.card').removeClass('c-faqs__card_gray');
        } else {
            jQuery('.card').addClass('c-faqs__card_gray');
            jQuery(this).removeClass('c-faqs__card_gray');
        }
    });

    jQuery('.js-actores-open').click(function (event) {
        event.preventDefault();
 
        openActor(jQuery(this));
    });
});

function openActor(target) {
    if (target.hasClass('open')) {
        closeActores();
        target.removeClass('open');
        target.next().slideUp();
    } else {
        closeActores();
        target.addClass('open');
        target.next().slideDown(); 
    }
}

function closeActores() {
    jQuery('.js-actores-open').removeClass('open');
    jQuery('.c-actores__mobile-item__bottom').slideUp();
}