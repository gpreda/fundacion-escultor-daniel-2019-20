/* eslint-disable */
jQuery(document).ready(function ($) {
    if (!Cookies.get('audiq8-id')) {
        jQuery('.c-cookies').addClass('c-cookies--show');
    }

    $('.js-close-cookies').click(function () {
        Cookies.set('audiq8-id', 1, {expires: 365, path: '/'});
        jQuery('.c-cookies').removeClass('c-cookies--show');
    });
});