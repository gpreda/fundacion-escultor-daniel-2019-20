/* eslint-disable */
jQuery(document).ready(function ($) {

    tllActor1 = new TimelineMax({paused: true});
    tl2Actor1 = new TimelineMax({paused: true, delay: 0.5});
    tl3Actor1 = new TimelineMax({paused: true, delay: 0.5});

    tllActor2 = new TimelineMax({paused: true});
    tl2Actor2 = new TimelineMax({paused: true, delay: 0.5});
    tl3Actor2 = new TimelineMax({paused: true, delay: 0.5});

    tllActor3 = new TimelineMax({paused: true});
    tl2Actor3 = new TimelineMax({paused: true, delay: 0.5});
    tl3Actor3 = new TimelineMax({paused: true, delay: 0.5});

    tllActor4 = new TimelineMax({paused: true});
    tl2Actor4 = new TimelineMax({paused: true, delay: 0.5});
    tl3Actor4 = new TimelineMax({paused: true, delay: 0.5});

    tllActor5 = new TimelineMax({paused: true});
    tl2Actor5 = new TimelineMax({paused: true, delay: 0.5});
    tl3Actor5 = new TimelineMax({paused: true, delay: 0.5});

    function getScreenWidth () {
        return window.innerWidth;
    }


    $('.c-actores__card--najwa').hover(function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor1, tl2Actor1, tl3Actor1, 'play');
        }
    }, function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor1, tl2Actor1, tl3Actor1, 'reverse');
        }
    });

    $('.c-actores__card--alberto').hover(function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor2, tl2Actor2, tl3Actor2, 'play');
        }
    }, function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor2, tl2Actor2, tl3Actor2, 'reverse');
        }
    });

    $('.c-actores__card--q8').hover(function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor3, tl2Actor3, tl3Actor3, 'play');
        }
    }, function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor3, tl2Actor3, tl3Actor3, 'reverse');
        }
    });

    $('.c-actores__card--javier').hover(function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor4, tl2Actor4, tl3Actor4, 'play');
        }
    }, function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor4, tl2Actor4, tl3Actor4, 'reverse');
        }
    });

    $('.c-actores__card--Kike').hover(function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor5, tl2Actor5, tl3Actor5, 'play');
        }
    }, function () {
        if (getScreenWidth() > 1024) {
            o = jQuery(this).find('.o-card');
            animateCard(o, tllActor5, tl2Actor5, tl3Actor5, 'reverse');
        }
    });

    // -----------------------------------------------

    $('.c-actores__card').click(function () {
        var actor = jQuery(this);
        var card = jQuery(this).find('.o-card');

        if(card.hasClass('mostrar')) {
            closeAll();
        } else {
            closeAll();
            actor.addClass('open');
            card.addClass('mostrar');
        }
    });
});

function animateCard(o, tl1, tl2, tl3, mode) {
    var oTop = o.find('.o-card__top');
    var oRight = o.find('.o-card__right');
    var oBottom = o.find('.o-card__bottom');
    var oLeft = o.find('.o-card__left');

    var oContainer = o.find('.o-card__container');

    var time = 0.3;

    tl1.to(oTop, time, {width: '100%'});
    tl1.to(oRight, time, {height: '100%'});
    tl1.to(oBottom, time, {width: '100%'});
    tl1.to(oLeft, time, {height: '100%'});
    tl2.to(o, time * 4, {backgroundColor: 'rgba(0, 0, 0, .5)', pointerEvents: 'auto'});
    tl3.to(oContainer, time * 4, {opacity: 1});

    if (mode == 'play') {
        tl1.play().timeScale(1);
        tl2.play().timeScale(1);
        tl3.play().timeScale(1);
    } else {
        tl1.reverse().timeScale(3);
        tl2.reverse().timeScale(4.5);
        tl3.reverse().timeScale(4.5);
    }
}

function closeAll() {

    jQuery('.o-card').removeClass('mostrar');
    jQuery('.c-actores__card').removeClass('open');
}