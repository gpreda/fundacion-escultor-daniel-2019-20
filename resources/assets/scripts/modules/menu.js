/* eslint-disable */
function startMenu() {
    //var lastScroll = 0;
    var delta = 700;

    jQuery(window).bind('scroll mousewheel', function (event) {
        var scroll = jQuery(window).scrollTop();

        if (scroll > delta) {
            if (event.originalEvent.wheelDelta >= 0) {
                jQuery('.c-header--fixed').addClass('open');
            }
            else {
                jQuery('.c-header--fixed').removeClass('open');
            }
        } else {
            jQuery('.c-header--fixed').removeClass('open');
        }
    });

    var hamburguer = jQuery('.js-hamburger');
    var menu = jQuery('.js-menu');
    var header_logo = jQuery('.c-header__logo');

    hamburguer.on('click', function (e) {
        e.preventDefault();

        if (hamburguer.hasClass('open')) {
            hamburguer.removeClass('open');
            menu.removeClass('show');
            header_logo.removeClass('menu-open');
        } else {
            hamburguer.addClass('open');
            menu.addClass('show');
            header_logo.addClass('menu-open');
        }
    });
}

module.exports = {
    startMenu: startMenu,
};

jQuery(document).ready(function ($) {

    var menu_children_link = jQuery('.menu-item-has-children > a');
    menu_children_link.append(' <i class="u-icon-chevron-down"></i>');

    var menu_children = jQuery('.menu-item-has-children');
    var submenu = jQuery('.sub-menu');

    menu_children.on('mouseover', function () {
        submenu.addClass('open-submenu-desktop');
    });

    menu_children.on('mouseout', function () {
        submenu.removeClass('open-submenu-desktop');
    });




});