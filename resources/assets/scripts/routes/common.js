import '../lib/js.cookie.min';
import '../lib/TweenMax.min';
import '../lib/photoswipe.min';
import '../lib/photoswipe-ui-default.min';
import '../lib/lazysizes.min';
import '../modules/carusel';
import menu from '../modules/menu';
import '../modules/timeline';
import '../modules/util';
import '../modules/actores';

export default {
    init() {
        // JavaScript to be fired on all pages
        menu.startMenu();
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
